﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main : MonoBehaviour
{
    public GameObject pokemonPrefab;
    public float perCentGap = 20;

    public float maxLineSize = 200;

    private float largestLineWidth;
    private float currentXCoord = 0, currentZCoord = 0;
    private bool firstRow = true;

    private List<GameObject> pokemonRow = new List<GameObject>();

    private int testCounter = 0;
   

    // Start is called before the first frame update
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PopulatePokemon(string nameIn, int weightIn, int heightIn, string colourIn)
    {
        //if (testCounter < 50)
        {
            if(testCounter == 192)
            { }

            GameObject pokemon = Instantiate(pokemonPrefab, new Vector3(0, 0, 0), Quaternion.identity);
            pokemon.name = "Pokemon" + testCounter + ": " + nameIn;
            PokemonSetup setup = pokemon.GetComponent<PokemonSetup>();

            float pokemonWidth = setup.SetCharacter(weightIn, heightIn, getColour(colourIn), nameIn);

            if (currentXCoord + pokemonWidth < maxLineSize || currentXCoord == 0)
            {         
                if (pokemonWidth > largestLineWidth)
                {
                    largestLineWidth = pokemonWidth;
                }

                if(!firstRow)
                {
                    pokemonRow.Add(pokemon);
                }
            }
            else
            {      
                if (firstRow)
                {
                    firstRow = false;
                }
                else
                {
                    //foreach(GameObject pok in pokemonRow)
                    //{
                    //    pok.transform.position = new Vector3(pok.transform.position.x, pok.transform.position.y,
                    //                                            currentZCoord + largestLineWidth * (largestLineWidth * (100 + perCentGap) / 100));
                    //}
                    //pokemonRow.Clear();                   
                }
                pokemonRow.Add(pokemon);
                largestLineWidth = pokemonWidth;
                currentZCoord += largestLineWidth * (100 + perCentGap) / 100;
                currentXCoord = 0;
            }
            setup.AlignPokemon(new Vector3(currentXCoord, 0, currentZCoord));
            currentXCoord += pokemonWidth + (pokemonWidth * perCentGap / 100);
            testCounter++;
        }
    }


    private Color getColour(string colourIn)
    {
        if (colourIn == "yellow")
            return Color.yellow;

        if (colourIn == "white")
            return Color.white;

        if (colourIn == "red")
            return Color.red;

        if (colourIn == "purple")
            return new Color(0.7f,0,1);

        if (colourIn == "pink")
            return new Color(1, 0.3f,0.58f);

        if (colourIn == "green")
            return Color.green;

        if (colourIn == "gray")
            return Color.gray;

        if (colourIn == "brown")
            return new Color(0.824f,0.41f,0.12f);

        if (colourIn == "blue")
            return Color.blue;

        if (colourIn == "black")
            return Color.black;

        else
        return Color.cyan;
    }
}
