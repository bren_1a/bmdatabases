﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Data.SQLite;

public class DatabaseScript : MonoBehaviour
{
    public Main main;

    // Start is called before the first frame update
    void Start()
    {
        print(Application.dataPath);
        SQLiteConnection connection = new SQLiteConnection("Data Source=\"" +
            Application.dataPath + "/veekun-pokedex.sqlite\";Version=3;");
        connection.Open();
        SQLiteCommand command = connection.CreateCommand();
        command.CommandType = System.Data.CommandType.Text;
        //command.CommandText = "select * from pokemon";
        command.CommandText = "Select pokemon.identifier, pokemon_colors.identifier, pokemon.weight, pokemon.height from pokemon, pokemon_species,pokemon_colors " +
            "where pokemon.species_id = pokemon_species.id and pokemon_species.color_id = pokemon_colors.id order by pokemon.height";
        var reader = command.ExecuteReader();
        //while (reader.Read())
        //{
        //    Debug.Log(reader["identifier"] + " has weight of " + reader.GetInt32(3));
        //}

        while (reader.Read())
        {
            Debug.Log(reader["identifier"] + " has weight of " + reader.GetInt32(2) + " and is " + reader.GetInt32(3) + " metres tall and is " + reader.GetString(1));

            main.PopulatePokemon(reader["identifier"].ToString(), reader.GetInt32(2), reader.GetInt32(3), reader.GetString(1));
        }
        connection.Close();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
