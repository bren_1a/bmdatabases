﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PokemonSetup : MonoBehaviour
{
    public GameObject pokemon;
    public GameObject text;

    public float weightHeightRatio = 40;

    private Color pokemonColour;
    private float width;
    private float height;
    private string pokemonName;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Sets the character and returns the width;
    public float SetCharacter(float weightIn, float heightIn, Color colourIn, string nameIn)
    {
        width = weightIn / heightIn / weightHeightRatio;
        height = heightIn;
        pokemonColour = colourIn;
        pokemonName = nameIn;

        pokemon.GetComponent<Renderer>().material.color = pokemonColour;
        pokemon.transform.localScale = new Vector3(width, height, width);        
        text.transform.position = new Vector3(text.transform.position.x, height / 2 + 1, text.transform.position.z);

        text.GetComponent<TextMesh>().text = pokemonName;

        return width;

    }

    public void AlignPokemon(Vector3 startPos)
    {
        this.transform.position = new Vector3(startPos.x + (width / 2), this.transform.position.y +
                                    height / 2, startPos.z);
    }
}
